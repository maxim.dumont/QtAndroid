#ifndef ADBWRAPPER_H
#define ADBWRAPPER_H

#include <QObject>
#include <QProcess>
#include <QMap>
#include <QString>
#include <QStringList>
#include <QList>
#include <QDebug>
#include <QRunnable>
#include <QFile>
#include "adbdevice.h"

class adbwrapper: public QObject
{
    Q_OBJECT
public:
    explicit adbwrapper(QObject *parent = 0);
    QList<adbdevice> deviceList();
    QList<QString> pushToPaths();
    void pushFile(QString &filePath,QString &target);
    void getState();
    void unlockBootloader();
    void rebootBootloader();
    void exitBootloader();
    void flashImage(QString &file);
private:
    void initMap();
    void initProcess(QString &processName);

    QMap<QString, QString> deviceProcesses;
    QByteArray processStandardOutput;
    QByteArray processStandardError;
signals:
    void processStatusPosted(QString standardOutput,QString standardError);
    void deviceStateRegistered(QString state);
};

#endif // ADBWRAPPER_H
