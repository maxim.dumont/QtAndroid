#include "adbwrapper.h"

adbwrapper::adbwrapper(QObject *parent):QObject(parent)
{
    initMap();
}

QList<adbdevice> adbwrapper::deviceList()
{
    auto command = deviceProcesses["adb:devices"];
    initProcess(command);
    QList<adbdevice> outputDevices;
    QString processOutputString(processStandardOutput);
    if(!processOutputString.isEmpty()){
        QStringList devices = processOutputString.split("\n", QString::SkipEmptyParts);
        for(auto device: devices){
            if(device!="List of devices attached"){
                adbdevice newDevice;
                newDevice.tokenizeDevice(device);
                outputDevices<<newDevice;
            }
        }
    }

    if(outputDevices.isEmpty()){
        adbdevice newDevice;
        newDevice.noDevice();
        outputDevices<<newDevice;
    }
    return outputDevices;
 }

QList<QString> adbwrapper::pushToPaths()
{
    QList<QString> pathResults;
    initProcess(deviceProcesses["shell:goto"]);
    QString processOutputString(processStandardOutput);
    for(auto path : processOutputString.split("\n")){
        pathResults<<path;
    }
    return pathResults;
}

void adbwrapper::pushFile(QString &filePath, QString &target)
{
    QFile file(filePath);
    file.open(QIODevice::ReadWrite);
    auto value = deviceProcesses["shell:push"].replace("{0}",file.fileName()).replace("{1}",target);
    qDebug()<<"ADB COMMAND"<<value;
    initProcess(value);

    emit processStatusPosted(QString(processStandardOutput),QString(processStandardError));
}

void adbwrapper::getState()
{
    initProcess(deviceProcesses["shell:state"]);
}

void adbwrapper::unlockBootloader()
{
    initProcess(deviceProcesses["fastboot:unlock"]);
}

void adbwrapper::rebootBootloader()
{
    initProcess(deviceProcesses["shell:reboot"]);
}

void adbwrapper::exitBootloader()
{
    initProcess(deviceProcesses["shell:exitloop"]);
}

void adbwrapper::flashImage(QString &file)
{
    auto value = deviceProcesses["fastboot:flash"].replace("{0}", file);
    initProcess(value);
}


void adbwrapper::initMap()
{
    deviceProcesses["adb:devices"] = "adb devices -l";
    deviceProcesses["shell:reboot"] = "adb reboot bootloader";
    deviceProcesses["shell:exitloop"] = "fastboot reboot";
    deviceProcesses["shell:goto"] = "adb shell ls /sdcard/";
    deviceProcesses["shell:push"] = "adb push {0} {1}";
    deviceProcesses["shell:state"] = "adb get-state";
    deviceProcesses["fastboot:unlock"] = "fastboot oem unlock";
    deviceProcesses["fastboot:flash"] = "fastboot flash system '{0}'";
}

void adbwrapper::initProcess(QString &processName)
{
    QProcess *process = new QProcess();
    process->start(processName);
    process->waitForFinished();
    processStandardOutput = process->readAllStandardOutput();
    processStandardError = process->readAllStandardError();
    process->close();

    emit processStatusPosted(QString(processStandardOutput),QString(processStandardError));
}
