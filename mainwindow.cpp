#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTableWidget>
#include <QHeaderView>
#include <QStringList>
#include <QTableWidgetItem>
#include <QModelIndex>
#include <QFileDialog>
#include "adbdevicedelegate.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->pushToDeviceBtn->setEnabled(false);
    ui->flashToDeviceBtnn->setEnabled(false);

    m_adb = new adbwrapper;

    refreshGrid();
    refreshDropDown();

    connect(m_adb,&adbwrapper::processStatusPosted,this, &MainWindow::onProcessStatusPosted);
    connect(m_adb,&adbwrapper::deviceStateRegistered,this, &MainWindow::onDeviceStateRegistered);

    connect(ui->unlockBootloaderBtn,&QPushButton::clicked, m_adb,&adbwrapper::unlockBootloader);
    connect(ui->rebootToBooloaderBtn,&QPushButton::clicked,m_adb,&adbwrapper::rebootBootloader);
    connect(ui->exitBootloaderBtn,&QPushButton::clicked,m_adb,&adbwrapper::exitBootloader);

    m_adb->getState();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_adb;
}

void MainWindow::on_actionQuit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_findDevicePushButton_clicked()
{
    refreshGrid();
    refreshDropDown();
}

void MainWindow::refreshGrid()
{
    auto devices = m_adb->deviceList();
    standardModel = new QStandardItemModel(devices.count(),5,this);
    standardModel->setHorizontalHeaderLabels(QStringList()<<"Model"<<"Device"<<"Product"<<"USB"<<"Serial"<<"Type");

    for(int i  =0;i<devices.count();i++){
        for(int j = 0;j<5;j++){
            QModelIndex index = standardModel->index(i,j,QModelIndex());
            standardModel->setData(index, devices[i].indexAt(j));
        }
    }
    ui->deviceList->setModel(standardModel);
    ui->deviceList->setItemDelegate(new adbdevicedelegate(this));
}

void MainWindow::refreshDropDown(){
    for(auto item : m_adb->pushToPaths()){
        ui->pushToDirComboBox->addItem("/sdcard/"+item);
    }
}

void MainWindow::setStatusBarForegroundColor(QString &input)
{
    if(input.contains("error")){
        ui->statusBar->setStyleSheet("color: rgb(239, 41, 41);");
    }else {
        ui->statusBar->setStyleSheet("color: rgb(78, 154, 6);");
    }
}

void MainWindow::on_getImageFileBtn_clicked()
{
    chosenImageFile = QFileDialog::getOpenFileName(this, tr("Open File"),"/home",tr(""));
    ui->imageFilePath->setText(chosenImageFile);
    ui->flashToDeviceBtnn->setEnabled(true);
}

void MainWindow::on_pushFileOpenBtn_clicked()
{
   chosenPushFile = QFileDialog::getOpenFileName(this, tr("Open File"),"/home",tr(""));
   ui->pushFileLineEdit->setText(chosenPushFile);
   ui->pushToDeviceBtn->setEnabled(true);
}

void MainWindow::on_pushToDeviceBtn_clicked()
{
    QString comboBoxValue = ui->pushToDirComboBox->currentText();
    ui->statusBar->showMessage("Pushing "+chosenPushFile+" to device location "+ui->pushToDirComboBox->currentText());
    m_adb->pushFile(chosenPushFile,comboBoxValue);
}

void MainWindow::onDeviceStateRegistered(QString messsage)
{
    ui->statusBar->showMessage(messsage);
    setStatusBarForegroundColor(messsage);
}

void MainWindow::onProcessStatusPosted(QString standardOutput, QString standardError)
{
    auto statusBarOutput = QString(standardOutput)+" "+QString(standardError);
    setStatusBarForegroundColor(statusBarOutput);
    ui->statusBar->showMessage(statusBarOutput);
}

void MainWindow::on_flashToDeviceBtnn_clicked()
{
    m_adb->flashImage(chosenPushFile);
}
