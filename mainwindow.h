#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <QListWidgetItem>
#include <QDebug>
#include <QStandardItemModel>
#include "adbwrapper.h"
#include "adbdevice.h"
#include "adbdevicedelegate.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void on_actionQuit_triggered();
    void on_findDevicePushButton_clicked();
    void on_getImageFileBtn_clicked();
    void on_pushFileOpenBtn_clicked();
    void on_pushToDeviceBtn_clicked();

    void onDeviceStateRegistered(QString messsage);
    void onProcessStatusPosted(QString standardOutput,QString standardError);
    void on_flashToDeviceBtnn_clicked();

private:
    Ui::MainWindow *ui;
    adbwrapper *m_adb;
    QStandardItemModel *standardModel;
    QString chosenPushFile,
            chosenImageFile;

    void refreshGrid();
    void refreshDropDown();
    void setStatusBarForegroundColor(QString &input);
};

#endif // MAINWINDOW_H
