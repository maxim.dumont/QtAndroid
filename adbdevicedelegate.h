#ifndef ADBDEVICEDELEGATE_H
#define ADBDEVICEDELEGATE_H

#include <QObject>
#include <QWidget>
#include <QStyleOptionViewItem>
#include <QModelIndex>
#include <QAbstractItemModel>
#include <QItemDelegate>
#include "adbdevice.h"
class adbdevicedelegate : public QItemDelegate
{
public:
  explicit adbdevicedelegate (QObject *parent = 0);
  QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const; //delegate editor (your custom widget)
  void setEditorData(QWidget *editor, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QAbstractItemModel *model,const QModelIndex &index) const; //transfer editor data to model
  void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option,const QModelIndex &index) const;
};

#endif // ADBDEVICEDELEGATE_H
