#ifndef ADBDEVICE_H
#define ADBDEVICE_H

#include <QString>
#include <QTextStream>
#include <QStringList>
#include <QTableWidgetItem>
class adbdevice
{
public:
    void tokenizeDevice(const QString &input);
    void noDevice();

    QString getModel() const;
    QString getDevice() const;
    QString getProduct() const;
    QString getUsb() const;
    QString getSerial() const;
    QString getType() const;
    QList<QString> getPushFoldersForDevice();
    QString indexAt(int &index);
protected:
private:
    QString model,device,product,usb, serial, type;
    bool noDeviceFlag;
};

#endif // ADBDEVICE_H
