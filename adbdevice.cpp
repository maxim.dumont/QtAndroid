#include "adbdevice.h"



void adbdevice::tokenizeDevice(const QString &input)
{
    QStringList splitInput = input.split(" ", QString::SplitBehavior::SkipEmptyParts);

    serial = splitInput[0];
    type = splitInput[1];
    usb = splitInput[2].split(":")[1];
//    product = splitInput[3].split(":")[1];
//    model = splitInput[4].split(":")[1];
//    device = splitInput[5].split(":")[1];
}

void adbdevice::noDevice()
{
    noDeviceFlag = true;
}

QString adbdevice::getModel() const
{
    return model;
}

QString adbdevice::getDevice() const
{
    return device;
}

QString adbdevice::getProduct() const
{
    return product;
}

QString adbdevice::getUsb() const
{
    return usb;
}

QString adbdevice::getSerial() const
{
    return serial;
}

QString adbdevice::getType() const
{
    return type;
}

QString adbdevice::indexAt(int &index)
{
    switch (index) {
    case 0:
        return model;
    case 1:
        return device;
    case 2:
        return product;
    case 3:
        return usb;
    case 4:
        return serial;
    case 5:
        return type;
    default:
        return QString{};

    }

    return QString{};
}


