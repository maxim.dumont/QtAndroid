#include "adbprocessor.h"

void adbprocessor::start(QString &process, QByteArray *ouputResult)
{
    QProcess *start = new QProcess();
    start->start(process);
    start->waitForFinished();
    ouputResult = new QByteArray(start->readAllStandardOutput());
}
